(ns clj-xero.services.organisations
  (:require [clj-xero.core :as core]
            [clj-xero.converters.timezone :as tz]))

(core/set-client! :organisations #{:get-all} *ns*
                  :result-fn (fn [r]
                               (->> r :organisations
                                    (map (fn [o]
                                           (assoc o :timezone-java (get tz/mapping (:timezone o))))))))
