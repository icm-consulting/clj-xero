(ns clj-xero.core-test
  (:require [clojure.test :refer :all]
            [clojure.edn :as edn]
            [clj-xero.core :refer :all]
            [clj-xero.services.accounts :as accounts]
            [clj-xero.services.bank-transactions :as bank-transactions]
            [clj-xero.services.bank-transfers :as bank-transfers]
            [clj-xero.services.contacts :as contacts]
            [clj-xero.services.invoices :as invoices]
            [clj-xero.services.journals :as journals]
            [clj-xero.services.organisations :as organisations]
            [clj-xero.services.payments :as payments]
            [clj-xero.services.receipts :as receipts]
            [clj-xero.services.reports :as reports]
            [clj-xero.services.tracking-categories :as tracking-categories]))

;; Enter credentials in a file named xero-private-access.edn in present working directory.
;; Write down consumer-key consumer-secret and pem-file like so:
;;    {:consumer-key    "CONSUMERKEY1231231231231231231"
;;     :consumer-secret "CONSUMERSECRET1231231231232312"
;;     :pem-file "temp/privatekey.pem"}
;; The credentials here should point to a Demo Company or similar in Xero.

(def credentials (let [{:keys [consumer-key consumer-secret pem-file]} (-> "xero-private-access.edn"
                                                                           slurp
                                                                           edn/read-string)]
                   (private-credentials consumer-key consumer-secret (slurp pem-file))))

(deftest test-creds
  (testing "Creds as first parameter"
    (is (vector? (accounts/get-all-accounts credentials))))
  (testing "Creds as wrapper"
    (is (vector? (with-credentials credentials (accounts/get-all-accounts))))))

(deftest test-accounts
  (testing "accounts"
    (is (vector? (accounts/get-all-accounts credentials))))
  (testing "single account"
    (is (map? (accounts/get-account-by-guid credentials nil)))))

(deftest test-bank-transactions
  (testing "bank-transactions"
    (is (vector? (bank-transactions/get-all-bank-transactions credentials))))
  (testing "single bank-transaction"
    (is (map? (bank-transactions/get-bank-transaction-by-guid credentials nil)))))

(deftest test-bank-transfers
  (testing "bank-transfers"
    (is (vector? (bank-transfers/get-all-bank-transfers credentials))))
  (testing "single bank-transfer"
    (is (map? (bank-transfers/get-bank-transfer-by-guid credentials nil)))))

(deftest test-contacts
  (testing "contacts"
    (is (vector? (contacts/get-all-contacts credentials))))
  (testing "single contact"
    (is (map? (contacts/get-contact-by-guid credentials nil)))))

(deftest test-invoices
  (testing "invoices"
    (is (vector? (invoices/get-all-invoices credentials))))
  (testing "single invoice"
    (is (map? (invoices/get-invoice-by-guid credentials nil)))))

(deftest test-journals
  (testing "journals"
    (is (seq? (journals/get-all-journals credentials))))
  (testing "single journal"
    (is (map? (journals/get-journal-by-guid credentials nil)))))

(deftest test-organisations
  (testing "organisations"
    (is (vector? (organisations/get-all-organisations credentials))))
  (testing "single organisation"
    (is (map? (organisations/get-organisation-by-guid credentials nil)))))

(deftest test-payments
  (testing "payments"
    (is (vector? (payments/get-all-payments credentials))))
  (testing "single payment"
    (is (map? (payments/get-payment-by-guid credentials nil)))))

(deftest test-receipts
  (testing "receipts"
    (is (vector? (receipts/get-all-receipts credentials))))
  (testing "single receipt"
    (is (map? (receipts/get-receipt-by-guid credentials nil)))))

(deftest test-tracking-categories
  (testing "trackingcategories"
    (is (vector? (tracking-categories/get-all-tracking-categories credentials))))
  (testing "single trackingcategory"
    (is (map? (tracking-categories/get-tracking-category-by-guid credentials nil)))))

(deftest test-reports
  (testing "bank-statements"
    (is (map? (->> (accounts/get-all-accounts credentials)
                   (filter #(= "BANK" (:bank-account-type %)))
                   first
                   :account-id
                   (#(reports/get-bank-statements credentials % {}))))))
  (testing "balance-sheet"
    (is (map? (reports/get-balance-sheet credentials {}))))
  (testing "profit-and-loss"
    (is (map? (reports/get-profit-and-loss credentials {}))))
  (testing "all-published-reports"
    (is (vector? (reports/get-published-reports credentials)))))

(comment
  (time (run-tests))
  (time (test-var #'test-creds))
  (time (test-var #'test-accounts))
  (time (test-var #'test-bank-transactions))
  (time (test-var #'test-bank-transfers))
  (time (test-var #'test-contacts))
  (time (test-var #'test-invoices))
  (time (test-var #'test-journals))
  (time (test-var #'test-organisations))
  (time (test-var #'test-payments))
  (time (test-var #'test-receipts))
  (time (test-var #'test-tracking-categories))
  (time (test-var #'test-reports)))