(ns clj-xero.converters.date
  (:require
    [clj-time.core :as time.core]
    [clj-time.coerce :as time.coerce]
    [clojure.string :as string]))

(def ^:private json-regex-fn
  (partial re-find #"(/Date\()(\d+)([\+\-]?\d*)(\)/)"))

(defn- extract-mills*
  [json-date]
  (some-> json-date
          (str)
          (json-regex-fn)
          (get 2)
          (Long/parseLong)))

;(defn- extract-offset*
;  [json->date]
;  (some-> json->date
;          (str)
;          (json-regex-fn)
;          (get 3)
;          (Long/parseLong)))

(defn json->date
  "Converts the Microsoft .NET JSON date format provided by some calls into a standard `java.util.Date` instance
  Note, one can optionally force the timezone used by providing a timezone id as a 2nd argument. This is useful
  for when the client sends a date as a UTC date which actually should be a date in the timezone of the organisation.
  (Timesheets come to mind here)"
  ([json-date]
   (json->date json-date nil))
  ([json-date timezone-id]
   (let [timezone (if timezone-id
                    (time.core/time-zone-for-id timezone-id)
                    time.core/utc)]
     (-> json-date
         (extract-mills*)
         (time.coerce/from-long)
         (time.core/from-time-zone timezone)
         (time.coerce/to-date)))))


(defn date->json
  "Converts the `java.util.Date` into the Microsoft .NET JSON date format.
  Note, one cat optionally force the timezone used by providing a timezone id as a 2nd argument. This is useful when Xero is expecting
  to push a date in the organisations timezone, but it's reading UTC time instances. (Timesheets come to mind) "
  ([date]
   (date->json date nil))
  ([date timezone-id]
   (let [timezone (if timezone-id
                    (time.core/time-zone-for-id timezone-id)
                    time.core/utc)]
     (format
       "/Date(%s)/"
       (-> date
           (time.coerce/to-date-time)
           (time.core/to-time-zone timezone)
           (time.coerce/to-local-date-time)
           (time.coerce/to-date)
           (.getTime))))))
