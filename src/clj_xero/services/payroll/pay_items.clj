(ns clj-xero.services.payroll.pay-items
  (:require [clj-xero.core :as core]))

(def api-url "https://api.xero.com/payroll.xro/1.0/")

(core/set-client! :pay-items #{:get :post} *ns*
                  :result-fn :pay-items
                  :api-url api-url)
