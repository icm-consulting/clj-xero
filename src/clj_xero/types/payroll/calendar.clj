(ns clj-xero.types.payroll.calendar)

(def weekly "WEEKLY")
(def fortnightly "FORTNIGHTLY")
(def four-weekly "FOURWEEKLY")
(def monthly "MONTHLY")
(def twice-monthly "TWICEMONTHLY")
(def quarterly "QUARTERLY")
