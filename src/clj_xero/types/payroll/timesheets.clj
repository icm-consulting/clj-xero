(ns clj-xero.types.payroll.timesheets
  (:require [clojure.set :as set]))

(def status-codes {:draft     "DRAFT"
                   :processed "PROCESSED"
                   :approved  "APPROVED"})

(defn ->status-code
  "Converts a keyword for the status into a value."
  [status-kw]
  (-> status-codes status-kw))

(defn ->status-kw
  "Converts a status value into a keyword."
  [status-val]
  (-> status-codes
      (set/map-invert)
      (get status-val)))
