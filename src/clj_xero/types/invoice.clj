(ns clj-xero.types.invoice)

(def invoice-types {:acc-pay "ACCPAY"
                    :acc-rec "ACCREC"})

(def status-codes {:draft      "DRAFT"
                   :submitted  "SUBMITTED"
                   :deleted    "DELETED"
                   :authorised "AUTHORISED"
                   :paid       "PAID"
                   :voided     "VOIDED"})

(def line-amount-types {:exclusive "Exclusive"
                        :inclusive "Inclusive"
                        :no-tax    "NoTax"})