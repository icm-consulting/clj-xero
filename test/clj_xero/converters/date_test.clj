(ns clj-xero.converters.date-test
  (:require
    [clojure.test :refer :all]
    [clj-xero.converters.date :as subject])
  (:import (java.util Date)))

(deftest json->date
  (testing "Valid JSON Date provided"
    (is (= (Date. 2000 1 1)
           (subject/json->date "/Date(60907554000000+0000)/"))))
  (testing "Invalid JSON Date provided"
    (is (nil? (subject/json->date "SomeInvalidData"))))
  (testing "Nil input"
    (is (nil? (subject/json->date nil)))))

(deftest date->json
  (testing "Valid Java Date"
    (is (= "/Date(60907554000000+0000)/"
           (subject/date->json (Date. 2000 1 1)))))
  (testing "Not a java.util.Date"
    (is (nil? (subject/date->json (Object.)))))
  (testing "Nil input"
    (is (nil? (subject/date->json nil)))))
