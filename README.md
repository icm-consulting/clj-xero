# clj-xero

A simple clojure wrapper to the [Xero](http://xero.com) API.

[API Docs](http://icm-consulting.bitbucket.org/clj-xero/)

## Usage

### Install

Leiningen:

[![Clojars Project](https://img.shields.io/clojars/v/clj-xero.svg)](https://clojars.org/clj-xero)

### Authentication / Authorisation

clj-xero supports private, public, and partner (the 3 P's) application authorisation schemes.

The end goal of each authentication/authorisation flow is to retrieve a set of ```credentials```, which are passed to each service call function.

All authorisation and credential functions are located in the ```clj-xero.core``` namespace.

#### Private applications
Private is the simplest authorisation scheme to use - no need to run through an oauth workflow in order to access Xero services.
Private authorisation involves creating credentials, using your private key that matches the public key registered with your Xero organisation that will be accessed.

```clj
(require '[clj-xero.core :as xero])

(def my-consumer-key "AAA222XXX")
(def my-consumer-secret "TOTALLYSECRET")
(def my-private-key (slurp "my-private-key.pem"))
(def credentials (xero/private-credentials my-consumer-key
                                           my-consumer-secret
                                           my-private-key))
```

#### Public applications
[Xero public authorisation](https://developer.xero.com/documentation/getting-started/public-applications/) involves taking your end users through the Xero oauth workflow to provide your app with authorisation to access their organisation.

```clj
;; 1. Create a public "consumer" with your consumer and secret keys
(def consumer (xero/public-consumer my-consumer-key my-secret-key))

;; 2. When you need to request authorisation - call this to retrieve the
;; URL to send the user to. Pass in the full callback URL that the user
;; will be sent to after they've authorised your app
(def request-token (xero/authorisation-request-token consumer "http://localhost:8080/callback"))
;; => {:authorisation-url "https://api.xero.com/oauth/Authorize..."
;;     :oath_token "BLAH"
;;     :oauth_token_secret "SHHH" ...}

;; 3. Once authorisation was received via your callback endpoint, generate
;; credentials which will be passed to each service function call.
;; The verifier token (the last arg) will be passed by Xero to your callback URL.
(def credentials (xero/public-authorised-credentials consumer request-token "VERIFIER_TOKEN"))

```

#### Partner applications
[Xero partner](https://developer.xero.com/documentation/getting-started/partner-applications/) authorisation workflow is similar to Public applications. You will need to provide the entrust certificate that Xero provides you with when setting up the partner application.

```clj
;; 1. Create a partner "consumer" with your consumer key and the private key of your application
(def consumer (xero/partner-consumer my-consumer-key
                                     my-private-key))
;; 2. Entrust keystore properties
(def entrust-keystore {:keystore "path/to/keystore" ;;or instance of java.security.KeyStore
                       :keystore-type "PKCS12"
                       :keystore-pass "bPeKdwY4RQjyhGtqj2MV"})

;; 2. Get the fetch token, with the authorisation URL. This differs slightly
;; from the public application; the path to the entrust keystore, and its
;; password, are passed as the 3rd argument
(def request-token (xero/authorisation-request-token consumer
                                                "http://localhost:3000/callback"
                                                entrust-keystore))

;; 3. After authorisation, fetch the credentials
(def credentials (xero/partner-authorised-credentials consumer request-token "VERIFIER_TOKEN" entrust-keystore))

```

### Throttling

TODO

### Services
* [Accounts](#markdown-header-accounts)
* [Bank Transactions](#markdown-header-bank-transactions)
* [Bank Transfers](#markdown-header-bank-transfers)
* [Contacts](#markdown-header-contacts)
* [Invoices](#markdown-header-invoices)
* [Journals](#markdown-header-journals)
* [Organizations](#markdown-header-organizations)
* [Payments](#markdown-header-payments)
* [Receipts](#markdown-header-receipts)
* [Reports](#markdown-header-reports)
* [Tracking Categories](#markdown-header-tracking-categories)

### Documentation

#### Minimum Viable Snippet
```clj
(ns sample.usage
  (:require [clj-xero.core :as xero]
            [clj-xero.services.accounts :refer [get-all-accounts]]
            [clj-xero.services.reports :refer [get-bank-statements]]))

(def credentials (xero/private-credentials "AAA222XXX-key"
                                           "TOTALLYSECRET-secret"
                                           (slurp "myprivatekey.pem")))
                                           
(defn get-accounts-and-bank-statements []
  (let [accounts (get-all-accounts credentials)
        bank-accounts (filter #(= "BANK" (:bank-account-type %)) accounts)
        bank-statements (map #(get-bank-statements credentials (:account-id %) {}) bank-accounts)]
    {:accounts accounts
     :bank-statements bank-statements}))
```

#### Credentials

Wrap all calls using `with-credentials` or pass as the first parameter.
```clj
(ns sample.usage
  (:require [clj-xero.core :as xero]
            [clj-xero.services.accounts :as accounts]
            [clj-xero.services.payments :as payments]))

(def credentials (xero/private-credentials "AAA222XXX-key"
                                           "TOTALLYSECRET-secret"
                                           (slurp "myprivatekey.pem")))

; Wrap api calls with credentials
(xero/with-credentials credentials {:accounts (accounts/get-all-accounts)
                                    :payments (payments/get-all-payments)})

; Pass credentials as first param
{:accounts (accounts/get-all-accounts credentials)
 :payments (payments/get-all-payments credentials)}
```

#### Accounts
```clj
(require '[clj-xero.services.accounts :refer [get-all-accounts get-account-by-guid]])

(def all-accounts (get-all-accounts credentials))

(def first-account (get-account-by-guid credentials (-> all-accounts first :account-id)))
```

#### Bank Transactions
```clj
(require '[clj-xero.services.bank-transactions :refer [get-all-bank-transactions
                                                       get-bank-transaction-by-guid]])

(def all-bank-transactions
  (get-all-bank-transactions credentials))


(def first-bank-transaction
  (get-bank-transaction-by-guid credentials (-> all-bank-transactions
                                                first
                                                :bank-transaction-id)))
```

#### Bank Transfers
```clj
(require '[clj-xero.services.bank-transfers :refer [get-all-bank-transfers
                                                    get-bank-transfer-by-guid]])

(def all-bank-transfers
  (get-all-bank-transfers credentials))

(def first-bank-transfer
  (get-bank-transfer-by-guid credentials (-> all-bank-transfers
                                             first
                                             :bank-transfer-id)))
```

#### Contacts
```clj
(require '[clj-xero.services.contacts :refer [get-all-contacts get-contact-by-guid]])

(def all-contacts (get-all-contacts credentials))

(def first-contact (get-contact-by-guid credentials (-> all-contacts first :contact-id)))
```

#### Invoices
```clj
(require '[clj-xero.services.invoices :refer [get-all-invoices get-invoice-by-guid]])

(def all-invoices (get-all-invoices credentials))

(def first-invoice (get-invoice-by-guid credentials (-> all-invoices first :invoice-id)))
```

#### Journal
```clj
(require '[clj-xero.services.journals :refer [get-all-journals get-journal-by-guid]])

(def all-journals (get-all-journals credentials))

(def first-journal (get-journal-by-guid credentials (-> all-journals first :journal-id)))
```

#### Organisations
```clj
(require '[clj-xero.services.organisations :refer [get-all-organisations get-organisation-by-guid]])

(def all-organisations (get-all-organisations credentials))

(def first-organisation (get-organisation-by-guid credentials (-> all-organisations first :organisation-id)))
```

#### Payments
```clj
(require '[clj-xero.services.payments :refer [get-all-payments get-payment-by-guid]])

(def all-payments (get-all-payments credentials))

(def first-payment (get-payment-by-guid credentials (-> all-payments first :payment-id)))
```

#### Receipts
```clj
(require '[clj-xero.services.receipts :refer [get-all-receipts get-receipt-by-guid]])

(def all-receipts (get-all-receipts credentials))

(def first-receipt (get-receipt-by-guid credentials (-> all-receipts first :receipt-id)))
```

#### Reports
```clj
(require '[clj-xero.services.accounts :refer [get-all-accounts]]
         '[clj-xero.services.reports :refer [get-bank-statements
                                             get-balance-sheet
                                             get-profit-and-loss
                                             get-published-reports
                                             get-published-report]])

(def first-bank-account-id
  (->> (get-all-accounts credentials)
       (filter #(= "BANK" (:bank-account-type %)))
       first
       :account-id))

(def bank-statements (get-bank-statements credentials first-bank-account-id {}))

(def balance-sheet (get-balance-sheet credentials {}))

(def profit-and-loss (get-profit-and-loss credentials {}))

(def all-published-reports (get-published-reports credentials))
```

#### Tracking Categories
```clj
(require '[clj-xero.services.tracking-categories :refer [get-all-tracking-categories get-tracking-category-by-guid]])

(def all-tracking-categories (get-all-tracking-categories credentials))

(def first-tracking-category (get-tracking-category-by-guid credentials (-> all-tracking-categories
                                                                            first
                                                                            :tracking-category-id)))
```

#### Attachments
##### Adding an attachment 
To add an attachment to a Xero object such as an Invoice. 
```clojure
;;Example for adding an attachment to an Invoice
  (:require [clj-xero.core :as xero]
            [clj-xero.services.invoices :as invoices])
            
;; Add an attachment to a Xero object using an inputstream 
(invoices/add-attachment! credentials invoice-guid file-name input-stream)

;; Add an attachment to a Xero object using a map for the inputstream containing a body and length
(invoices/add-attachment! credentials invoice-guid file-name {:body input-stream :length file-length})
```

## Change Log

### 0.5.3
- Add function for refreshing partner credentials

### 0.5.6
- Add ability to set User-Agent in http header required checkpoint for xero partner

### 0.5.7
- Fixing http status 411 error (no content length set) when adding an attachment to a xero object by allowing the length to be passed through with the inputstream as a map

## License

Copyright © 2016 [ICM Consulting Pty Ltd]("http://www.icm-consulting.com.au")

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.

[1]: http://blog.fogus.me/2012/08/23/minimum-viable-snippet/
