(ns clj-xero.services.payroll.calendars
  (:require [clj-xero.core :as core]))

(def api-url "https://api.xero.com/payroll.xro/1.0/")

(core/set-client! :payroll-calendars #{:get} *ns*
                    :result-fn :payroll-calendars
                    :api-url api-url)

