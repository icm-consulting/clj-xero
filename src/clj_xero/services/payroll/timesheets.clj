(ns clj-xero.services.payroll.timesheets
  (:require [clj-xero.core :as core]))

(def api-url "https://api.xero.com/payroll.xro/1.0/")

(core/set-client! :timesheets #{:get-all} *ns*
                  :api-url api-url
                  :result-fn :timesheets)

;; Seems that version 2.0 of the API will return a list of items for get-by-guid, however timesheets being a
;; version 1.0 API returns a single instance, but our get-by-guid generated function assumes that we will
;; have a sequence and it pulls the first one, so we need to "mangle the data to do that.
(core/set-client! :timesheets #{:get-by-guid} *ns*
                  :api-url api-url
                  :result-fn (fn [body]
                               (-> body
                                   :timesheet
                                   (vector))))


;; Timesheets can only be pushed in XML according to Xero API Support emails I've received.
;; So we need to handle the 'conversion' of the timesheet map structure into an xml structure.
;; Note however, we are sending a post of XML, but requesting JSON response, which seems to work 100%
(core/set-client! :timesheets #{:post} *ns*
                  :api-url api-url
                  :decorate-fn identity
                  :request-body-fn identity
                  :result-fn (fn [result]
                               (-> result :timesheets (first))))