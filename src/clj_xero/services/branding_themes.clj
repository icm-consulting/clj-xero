(ns clj-xero.services.branding-themes
  (:require [clj-xero.core :as core]))

(core/set-client! :branding-themes #{:get :post} *ns*)
